import THREE from "three";

export const MAIN_LIGHT_COLOR = 0xffffaa;
export const FILL_LIGHT_COLOR = 0xff0000;

export const ASTEROIDS = 200;
export const ASTEROIDS_SIZE = 2000;

export const ANGULAR_ACCELERATION = 0.0025;
export const LINEAR_ACCELERATION = 0.005;
export const TARGET_ACCELERATION = 0.01;
export const FRICTION = 0.9;

export const PROJECTILE_VELOCITY = 5;
export const PROJECTILE_DURATION = 1000;

export const NEGATIVE = -1;
export const POSITIVE = 1;

export const DEBRIS = 200;
export const DEBRIS_DISTANCE = 100;

export const AXIS_X = new THREE.Vector3(1,0,0);
export const AXIS_Y = new THREE.Vector3(0,1,0);
export const AXIS_Z = new THREE.Vector3(0,0,1);

export const PATH = "http://rojo2.com/labs/webgl/elite/";

//export const HUD_COLOR = 0xff7700;
//export const HUD_COLOR = 0x00ccff;
export const HUD_COLOR = 0x00ffcc;
export const HUD_OPACITY = 0.75;

export const HUD_RADAR_SIZE = 256;
export const HUD_RADAR_HALFSIZE = HUD_RADAR_SIZE * 0.5;
export const HUD_RADAR_POSITION = HUD_RADAR_HALFSIZE + 32;
export const HUD_RADAR_LIMIT = 0.975;

export const HUD_DOT_SIZE = 2;
export const HUD_ENEMY_DOT_SIZE = 4;

export const HUD_STATIC_COLOR = HUD_COLOR;
export const HUD_ENEMY_COLOR = 0xff00000;
export const HUD_FRIEND_COLOR = 0x0000ff;

export const MIN_FIRE_DISTANCE = 300;
