import THREE from "three";
import {
  ANGULAR_ACCELERATION,
  LINEAR_ACCELERATION,
  TARGET_ACCELERATION,
  FRICTION
} from "./constants";

// TODO: Realmente todos estos parámetros se pueden convertir en parámetros
// cacheables a través de getters que lo que hacen es reunir los valores
// a través de la suma de los parámetros de otros componentes como podrían
// ser impulsores, motor, etc.
//
// En realidad esto más que ship, debería llamarse ShipModel ya que es la
// representación de la nave y ShipController, debería ser Ship.
export class ShipModel {
  constructor() {
    this.angularAcceleration = new THREE.Vector3(
      ANGULAR_ACCELERATION,
      ANGULAR_ACCELERATION,
      ANGULAR_ACCELERATION
    );

    this.linearAcceleration = new THREE.Vector3(
      LINEAR_ACCELERATION,
      LINEAR_ACCELERATION,
      LINEAR_ACCELERATION
    );

    this.targetAcceleration = new THREE.Vector3(
      TARGET_ACCELERATION,
      TARGET_ACCELERATION,
      TARGET_ACCELERATION
    );

    this.angularFriction = new THREE.Vector3(
      FRICTION,
      FRICTION,
      FRICTION
    );

    this.linearFriction = new THREE.Vector3(
      FRICTION,
      FRICTION,
      FRICTION
    );

    this.maxCargo = 32;
    this.maxVelocity = 2.0; // TODO: Utilizar este parámetro para el control de la velocidad (hacia adelante).
    this.boost = 3.0;
  }
}

export default ShipModel;
