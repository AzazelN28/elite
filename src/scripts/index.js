import THREE from "three";
import Key from "./Keyboard";
import Debris from "./Debris";
import {
  MAIN_LIGHT_COLOR,
  FILL_LIGHT_COLOR,
  AXIS_X,
  AXIS_Y,
  AXIS_Z,
  NEGATIVE,
  POSITIVE,
  ASTEROIDS,
  ASTEROIDS_SIZE,
  PATH,
  HUD_COLOR,
  HUD_OPACITY,
  HUD_RADAR_LIMIT,
  HUD_RADAR_SIZE,
  HUD_RADAR_HALFSIZE,
  HUD_RADAR_POSITION,
  HUD_DOT_SIZE,
  HUD_ENEMY_DOT_SIZE,
  HUD_STATIC_COLOR,
  HUD_ENEMY_COLOR,
  HUD_FRIEND_COLOR
} from "./constants";

import * as Random from "./Random";
import ShipModel from "./ShipModel";
import Ship from "./Ship";
import ShipAI from "./ShipAI";
import Projectiles from "./Projectiles";
import CubeTextureLoader from "./CubeTextureLoader";
import OBJLoader from "./OBJLoader";

const debug = document.querySelector(".debug");

// TODO: Estaría de puta madre reorganizar el código para que no sea este
// churro inmenso de mierda y también molaría implementar algún tipo de
// físicas. Por lo que he visto en casi todos los juegos del espacio lo que hacen es
// tener varios modelos de detalle. Normalmente tienen lo que se llama la "hit box"
// que lo que hace es averiguar si un disparo se encuentra dentro de la caja
// si un disparo se encuentra dentro de la caja, entonces comprueban que haya colisionado
// o bien con otras cajas de la naves o bien con un convex hull, es decir, un modelo
// simplificado y convexo de las naves. El convex hull no es más que otro modelo
// 3D sin textura y sin material, totalmente invisible.

class Game {
  static rand() {
    return (Math.random() - 0.5) * 2.0;
  }

  static rands() {
    const value = Game.rand();
    if (value === 0) {
      return 1;
    }
    return (value < 0) ? Math.floor(value) : Math.ceil(value);
  }

  static randm(max,min) {
    const dist = (Math.random() * (max - min)) + min;
    return Math.sin(Math.random() * 2 * Math.PI) * dist;
  }

  constructor(canvas = document.querySelector("canvas")) {

    const objLoader = new OBJLoader();
    objLoader.setPath(PATH);

    this.lastNow = 0;

    this.music = document.createElement("audio");
    this.music.src = PATH + "music1.mp3";
    this.music.loop = true;
    this.music.volume = 0.75;
    this.music.play();

    this.engine = document.createElement("audio");
    this.engine.src = PATH + "engine.mp3";
    this.engine.loop = true;
    this.engine.volume = 0.0;
    this.engine.play();

    this.audios = [
      this.music,
      this.engine
    ];

    this.canvas = canvas;
    this.canvas.width = canvas.clientWidth;
    this.canvas.height = canvas.clientHeight;
    this.deactivate = this.deactivate.bind(this);
    this.activate = this.activate.bind(this);
    this.frame = this.frame.bind(this);
    this.destroySound = this.destroySound.bind(this);
    this.frameID = null;
    this.resize = this.resize.bind(this);
    this.beforeUnload = this.beforeUnload.bind(this);
    this.renderer = new THREE.WebGLRenderer({ canvas });

    const backgroundTexture = new CubeTextureLoader();
    backgroundTexture.setPath(PATH).load("bomaye.png");

    this.scene = new THREE.Scene();
    this.scene.fog = new THREE.FogExp2(0x251b13,0.001);
    this.scene.background = backgroundTexture.texture;
    this.hud = new THREE.Scene();

    this.camera = new THREE.PerspectiveCamera( 75, this.canvas.width / this.canvas.height, 0.1, 1000000.0 );
    this.camera.position.z = 5;

    const width = this.canvas.width * 0.5;
    const height = this.canvas.height * 0.5;
    this.hudCamera = new THREE.OrthographicCamera( -width, width, -height, height, -1, 1 );

    const hudLoader = new THREE.TextureLoader();

    /**
     * Puntero/Mirilla/Cruceta.
     */
    this.hudCenterBorders = new THREE.Sprite(new THREE.SpriteMaterial({
      map: hudLoader.setPath(PATH).load("hud-center-border.png"),
      color: HUD_COLOR
    }));
    this.hudCenterBorders.material.transparent = true;
    this.hudCenterBorders.material.opacity = HUD_OPACITY;
    this.hudCenterBorders.scale.set(512,512,512);
    this.hud.add(this.hudCenterBorders);

    /**
     * Hud Center Movement 1
     */
    this.hudCenterMovement1 = new THREE.Sprite(new THREE.SpriteMaterial({
      map: hudLoader.load("hud-center-mov1.png"),
      color: HUD_COLOR
    }));
    this.hudCenterMovement1.material.transparent = true;
    this.hudCenterMovement1.material.opacity = HUD_OPACITY;
    this.hudCenterMovement1.scale.set(512,512,512);
    this.hud.add(this.hudCenterMovement1);

    /**
     * Hud Center Movement 2
     */
    this.hudCenterMovement2 = new THREE.Sprite(new THREE.SpriteMaterial({
      map: hudLoader.load("hud-center-mov2.png"),
      color: HUD_COLOR
    }));
    this.hudCenterMovement2.material.transparent = true;
    this.hudCenterMovement2.material.opacity = HUD_OPACITY;
    this.hudCenterMovement2.scale.set(512,512,512);
    this.hud.add(this.hudCenterMovement2);

    /**
     * Cross hair
     */
    this.hudCrossHair = new THREE.Sprite(new THREE.SpriteMaterial({
      map: hudLoader.load("hud-center-crosshair.png"),
      color: HUD_COLOR
    }));
    this.hudCrossHair.material.transparent = true;
    this.hudCrossHair.material.opacity = HUD_OPACITY;
    this.hudCrossHair.scale.set(512,512,512);
    this.hud.add(this.hudCrossHair);

    /**
     * Radar frontal de la nave.
     */
    this.hudFront = new THREE.Sprite(new THREE.SpriteMaterial({
      map: hudLoader.load("hud-radar.png"),
      color: HUD_COLOR
    }));
    this.hudFront.material.transparent = true;
    this.hudFront.material.opacity = HUD_OPACITY;
    this.hudFront.position.set(width - HUD_RADAR_HALFSIZE, -height + HUD_RADAR_HALFSIZE,0);
    this.hudFront.scale.set(HUD_RADAR_SIZE,HUD_RADAR_SIZE,1);
    this.hud.add(this.hudFront);

    /**
     * Radar trasero de la nave
     */
    this.hudBack = new THREE.Sprite(new THREE.SpriteMaterial({
      map: hudLoader.load("hud-radar.png"),
      color: HUD_COLOR
    }));
    this.hudBack.material.transparent = true;
    this.hudBack.material.opacity = HUD_OPACITY;
    this.hudBack.position.set(-width + HUD_RADAR_HALFSIZE,-height + HUD_RADAR_HALFSIZE,0);
    this.hudBack.scale.set(HUD_RADAR_SIZE,HUD_RADAR_SIZE,1);
    this.hud.add(this.hudBack);

    this.hudStatus = new THREE.Sprite(new THREE.SpriteMaterial({
      map: hudLoader.load("hud-shield-status.png"),
      color: HUD_COLOR
    }));
    this.hudStatus.material.transparent = true;
    this.hudStatus.material.opacity = HUD_OPACITY;
    this.hudStatus.material.rotation = Math.PI;
    this.hudStatus.position.set(-width + HUD_RADAR_HALFSIZE, height + HUD_RADAR_HALFSIZE + 32, 0);
    this.hudStatus.scale.set(HUD_RADAR_SIZE,HUD_RADAR_SIZE,1);
    this.hud.add(this.hudStatus);

    this.hudTargetVelocity = new THREE.Sprite(
      new THREE.SpriteMaterial({
        map: hudLoader.load("hud-arrow.png"),
        color: HUD_COLOR
      })
    );
    this.hudTargetVelocity.material.transparent = true;
    this.hudTargetVelocity.material.opacity = HUD_OPACITY;
    this.hudTargetVelocity.position.set(0,0,0);
    this.hudTargetVelocity.scale.set(-32,32,32);
    this.hud.add(this.hudTargetVelocity);

    this.hudCurrentVelocity = new THREE.Sprite(
      new THREE.SpriteMaterial({
        map: hudLoader.load("hud-arrow.png"),
        color: HUD_COLOR
      })
    );
    this.hudCurrentVelocity.material.transparent = true;
    this.hudCurrentVelocity.material.opacity = HUD_OPACITY;
    this.hudCurrentVelocity.position.set(0,0,0);
    this.hudCurrentVelocity.scale.set(32,32,32);
    this.hud.add(this.hudCurrentVelocity);

    this.hudLogo = new THREE.Sprite(
      new THREE.SpriteMaterial({
        map: hudLoader.load("rojo2.png"),
        color: HUD_COLOR
      })
    );
    this.hudLogo.material.transparent = true;
    this.hudLogo.material.opacity = HUD_OPACITY;
    this.hudLogo.material.rotation = Math.PI;
    this.hudLogo.position.set(-width + 64, height - 64, 0);
    this.hudLogo.scale.set(64,64,1);
    this.hud.add(this.hudLogo);

    /**
     * Creamos la clase encargada de gestionar el Debris.
     */
    this.debris = new Debris(this.scene);

    // TODO: Sería interesante mover el comportamiento de crear
    // y destruir proyectiles a una clase llamada "Projectiles"
    // que como parámetro reciba la escena y un número de objetos
    // fijo. Ésta clase reuniría dos propósitos, por una parte
    // crear un ObjectPool capaz de reutilizar objetos proyectil
    // y por otra encargarse de añadir y eliminar los proyectiles
    // de la escena.
    this.projectiles = new Projectiles(this.scene, this);

    this.ship = new Ship(this.camera);

    const lod = new THREE.LOD();
    objLoader.load("enemy.obj", (obj) => {
      obj.children[0].material.map = (new THREE.TextureLoader().setPath(PATH).load("enemy_uv.png"));
      lod.addLevel(obj, 0);
    });

    /*const l0 = new THREE.Mesh(
      new THREE.IcosahedronGeometry(5, 3),
      new THREE.MeshPhongMaterial({ color: 0xff3333 })
    );

    const l1 = new THREE.Mesh(
      new THREE.IcosahedronGeometry(5, 2),
      new THREE.MeshPhongMaterial({ color: 0xff3333 })
    );

    const l2 = new THREE.Mesh(
      new THREE.IcosahedronGeometry(5, 1),
      new THREE.MeshPhongMaterial({ color: 0xff3333 })
    );

    const l3 = new THREE.Mesh(
      new THREE.IcosahedronGeometry(5, 0),
      new THREE.MeshPhongMaterial({ color: 0xff3333 })
    );

    const l4 = new THREE.Sprite();
    l4.scale.x = l4.scale.y = l4.scale.z = 5;*/

    // TODO: Sería interesante poder mover estas distancias
    // a una formula que utilice no sólo el nivel de detalle
    // si no el tamaño del objeto.
    /*lod.addLevel(l4, 500);
    lod.addLevel(l3, 250);
    lod.addLevel(l2, 100);
    lod.addLevel(l1, 50);
    lod.addLevel(l0, 25);*/

    // TODO: Algo similar ocurriría con la clase "Ships", que se
    // encargaría de gestionar las naves que se añaden y se quitan
    // de la escena.
    this.ships = [
      new ShipAI(
        new Ship(lod),
        this
      )
    ];

    this.ships[0].target.target.position.set(ASTEROIDS_SIZE,ASTEROIDS_SIZE,ASTEROIDS_SIZE);

    // naves que se muestran en el hud.
    this.hudMarkers = [];
    this.hudShips = [];
    for (let ship of this.ships) {
      const hudShip = new THREE.Sprite(new THREE.SpriteMaterial({color: HUD_ENEMY_COLOR}));
      hudShip.scale.set(HUD_ENEMY_DOT_SIZE,HUD_ENEMY_DOT_SIZE,HUD_ENEMY_DOT_SIZE);
      this.hudShips.push(hudShip);
      const hudMarker = new THREE.Sprite(new THREE.SpriteMaterial({
        map: (new THREE.TextureLoader().setPath(PATH).load("hud-marker.png")),
        color: HUD_ENEMY_COLOR
      }));
      hudMarker.scale.set(64,64,64);
      this.hudMarkers.push(hudMarker);
      this.hud.add(hudShip);
      this.hud.add(hudMarker);
      this.scene.add(ship.target.target);
    }

    const ambientLight = new THREE.AmbientLight( 0x000000 );
    const dlight1 = new THREE.DirectionalLight( MAIN_LIGHT_COLOR, 1 );
    dlight1.position.set(
      -0.945144206051346,
      0.31942386941133627,
      0.06834340800585827
    );

    const dlight2 = new THREE.DirectionalLight( FILL_LIGHT_COLOR, 0.125 );
    dlight2.position.set(
      0.6733409200708829,
      -0.20182288170689072,
      0.7112520859566087
    );
    this.scene.add( ambientLight );
    this.scene.add( dlight1 );
    this.scene.add( dlight2 );

    this.asteroids = [];
    this.hudAsteroids = [];

    // TODO: Lo suyo sería poder utilizar modelos más o menos molones
    // para los asteroides.
    Random.reset(0);
    for (let index = 0; index < ASTEROIDS; index++) {
      /*const asteroidGeometry = new THREE.IcosahedronGeometry( 1 + (Random.value() * 50), 0 );
      const asteroid = new THREE.Mesh( asteroidGeometry, material );*/

      const asteroid = new THREE.LOD();

      asteroid.position.set(Random.unit() * ASTEROIDS_SIZE, Random.unit() * ASTEROIDS_SIZE, Random.unit() * ASTEROIDS_SIZE);
      asteroid.rotation.set(Random.angle(), Random.angle(), Random.angle());
      const scale = Random.between(5,75);
      asteroid.scale.set(scale,scale,scale);

      asteroid.userData.rotX = Random.unit() * 0.01;
      asteroid.userData.rotY = Random.unit() * 0.01;
      //asteroid.userData.collider = new THREE.Sphere(asteroid.position, scale);
      this.scene.add( asteroid );

      this.asteroids.push( asteroid );

      const asteroidHud = new THREE.Sprite(new THREE.SpriteMaterial({ color: HUD_STATIC_COLOR }));
      asteroidHud.scale.set(HUD_DOT_SIZE,HUD_DOT_SIZE,HUD_DOT_SIZE);
      this.hud.add( asteroidHud );

      this.hudAsteroids.push( asteroidHud );
    }

    // TODO: Arreglar esta mierda de alguna manera. Me sangran los ojos
    // sólo de ver esta PEDAZO DE CHAPUZA.
    objLoader.load("asteroid_001_low.obj", (obj) => {
      for (let asteroid of this.asteroids) {
        asteroid.addLevel(obj.clone(), 20 * asteroid.scale.x);
      }
      objLoader.load("asteroid_001_mid.obj", (obj) => {
        for (let asteroid of this.asteroids) {
          asteroid.addLevel(obj.clone(), 15 * asteroid.scale.x);
        }
        objLoader.load("asteroid_001_mid2.obj", (obj) => {
          for (let asteroid of this.asteroids) {
            asteroid.addLevel(obj.clone(), 10 * asteroid.scale.x);
          }
          objLoader.load("asteroid_001_high.obj", (obj) => {
            for (let asteroid of this.asteroids) {
              asteroid.addLevel(obj.clone(), 5 * asteroid.scale.x);
            }
          });
        });
      });
    });

    this.start();
  }

  activate() {
    for (let audio of this.audios) {
      audio.play();
    }
  }

  deactivate() {
    for (let audio of this.audios) {
      audio.pause();
    }
  }

  start() {
    this.requestFrame();

    window.addEventListener("beforeunload", this.beforeUnload);
    window.addEventListener("resize", this.resize);
    document.addEventListener("visibilitychange", () => {
      if (document.hidden) {
        this.deactivate();
      } else {
        this.activate();
      }
    });
    this.resize();
  }

  stop() {
    this.cancelFrame();
  }

  requestFrame() {
    this.frameID = window.requestAnimationFrame(this.frame);
    return true;
  }

  cancelFrame() {
    if (this.frameID === null) {
      return false;
    }
    window.cancelAnimationFrame(this.frameID);
    this.frameID = null;
    return true;
  }

  frame(now) {
    this.input();
    this.update(now);
    this.lastNow = now;
    this.render();
    this.requestFrame();
  }

  getFirstConnectedGamepad() {
    const gamepads = navigator.getGamepads();
    for (let index = 0; index < gamepads.length; index++) {
      const gamepad = gamepads[index];
      if (gamepad && gamepad.connected) {
        return gamepad;
      }
    }
    return null;
  }

  input() {

    /**
     * Comportamiento con teclado.
     */
    if (Key.isPressed(Key.UP)) {
      this.ship.yaw(NEGATIVE);
    } else if (Key.isPressed(Key.DOWN)) {
      this.ship.yaw(POSITIVE);
    }

    if (Key.isPressed(Key.LEFT)) {
      this.ship.roll(POSITIVE);
    } else if (Key.isPressed(Key.RIGHT)) {
      this.ship.roll(NEGATIVE);
    }

    if (Key.isPressed(Key.Q)) {
      this.ship.pitch(POSITIVE);
    } else if (Key.isPressed(Key.E)) {
      this.ship.pitch(NEGATIVE);
    }

    if (Key.isPressed(Key.W)) {
      this.ship.move(NEGATIVE);
    } else if (Key.isPressed(Key.S)) {
      this.ship.move(POSITIVE);
    }

    if (Key.isPressed(Key.D)) {
      this.ship.strafeX(POSITIVE);
    } else if (Key.isPressed(Key.A)) {
      this.ship.strafeX(NEGATIVE);
    }

    if (Key.isPressed(Key.R)) {
      this.ship.strafeY(POSITIVE);
    } else if (Key.isPressed(Key.F)) {
      this.ship.strafeY(NEGATIVE);
    }

    if (Key.isPressed(Key.SPACE)) {
      if (this.ship.fire()) {
        // creamos los projectiles para esta nave.
        this.addProjectiles(this.ship);
      }
    }

    /**
     * Comportamiento con mando.
     */
    const gamepad = this.getFirstConnectedGamepad();
    if (gamepad) {

      /**
       * Layout para el mando GPX de Thrustmaster (en Linux)
       */
      // Stick izquierdo.
      if (Math.abs(gamepad.axes[1]) > 0.1) {
        this.ship.yaw(gamepad.axes[1]);
      }

      if (Math.abs(gamepad.axes[0]) > 0.1) {
        this.ship.roll(-gamepad.axes[0]);
      }

      // Stick derecho.
      if (Math.abs(gamepad.axes[3]) > 0.1) {
        this.ship.pitch(-gamepad.axes[3]);
      }

      if (Math.abs(gamepad.axes[4]) > 0.1) {
        this.ship.strafeY(-gamepad.axes[4]);
      }

      if (gamepad.buttons[4].value > 0) {
        this.ship.move(POSITIVE);
      } else if (gamepad.buttons[5].value > 0) {
        this.ship.move(NEGATIVE);
      }

      if (gamepad.buttons[0].value > 0) {
        if (this.ship.fire()) {
          // creamos los projectiles para esta nave.
          this.addProjectiles(this.ship);
        }
      }

      if (gamepad.buttons[7].value > 0) {
        console.log(gamepad);
      }

      /**
       * Layout para el mando GPX de Thrustmaster (en Windows)
       */
      /*
      // Stick izquierdo.
      if (Math.abs(gamepad.axes[1]) > 0.1) {
        this.ship.yaw(gamepad.axes[1]);
      }

      if (Math.abs(gamepad.axes[0]) > 0.1) {
        this.ship.roll(-gamepad.axes[0]);
      }

      // Stick derecho.
      if (Math.abs(gamepad.axes[2]) > 0.1) {
        this.ship.pitch(-gamepad.axes[2]);
      }

      if (Math.abs(gamepad.axes[3]) > 0.1) {
        this.ship.strafeY(-gamepad.axes[3]);
      }

      if (gamepad.buttons[4].value > 0) {
        this.ship.move(POSITIVE);
      } else if (gamepad.buttons[5].value > 0) {
        this.ship.move(NEGATIVE);
      }

      if (gamepad.buttons[7].value > 0) {
        if (this.ship.fire()) {
          // creamos los projectiles para esta nave.
          this.addProjectiles(this.ship);
        }
      }
      */
    }
  }

  addProjectiles(ship, distance = 1.0) {
    this.projectiles.create(ship);
    this.addFireSound(distance);
  }

  addFireSound(distance) {
    this.audios.push(this.createFireSound(distance));
  }

  createFireSound(volume) {
    const audio = document.createElement("audio");
    audio.src = PATH + "shot.wav";
    if (!isNaN(volume) && isFinite(volume)) {
      audio.volume = Math.max(0.1,volume);
    }
    audio.addEventListener("ended", this.destroySound);
    audio.play();
    return audio;
  }

  destroySound(e) {
    const audio = e.target;
    audio.removeEventListener("ended", this.destroySound);
    const index = this.audios.indexOf(audio);
    if (index >= 0) {
      this.audios.splice(index, 1);
    } else {
      console.warn("Audio not found");
    }
  }

  createAsteroidHitSound() {
    const audio = document.createElement("audio");
    audio.src = PATH + "hit.wav";
    audio.volume = 1.0;
    audio.addEventListener("ended", this.destroySound);
    audio.play();
    return audio;
  }

  addAsteroidHitSound() {
    const count = this.audios.reduce((initial, current) => {
      if (current.src.indexOf("hit.wav") >= 0) {
        return initial + 1;
      }
      return initial;
    }, 0);

    if (count < 1) {
      this.audios.push(this.createAsteroidHitSound());
    }
  }

  update(now) {
    if (this.canvas.width !== this.canvas.clientWidth
     || this.canvas.height !== this.canvas.clientHeight) {
      this.resize();
    }

    /**
     * Actualizamos el jugador.
     */
    // actualizamos la nave.
    this.ship.update();

    this.engine.volume = Math.min(1.0, Math.max(0.0, Math.abs(this.ship.relativeCurrentVelocity)));

    /**
     * Actualizamos la interfaz.
     */
    this.hudCenterMovement1.position.set(
      this.ship.angularVelocity.y * 512,
      this.ship.angularVelocity.x * 512,
      0
    );

    this.hudCenterMovement2.position.set(
      this.ship.angularVelocity.y * 1024,
      this.ship.angularVelocity.x * 1024,
      0
    );

    this.hudCrossHair.position.set(
      this.ship.angularVelocity.y * 2048,
      this.ship.angularVelocity.x * 2048,
      0
    );

{
    const velocityAngle = Math.min(1.0, Math.max(-1.0, this.ship.relativeTargetVelocity * 0.8));
    this.hudTargetVelocity.position.set(
      Math.cos( velocityAngle ) * 256,
      Math.sin( velocityAngle ) * 256,
      0
    );

    this.hudTargetVelocity.material.rotation = velocityAngle;
}

{
    const velocityAngle = Math.min(1.0, Math.max(-1.0,this.ship.relativeCurrentVelocity * 0.8));
    this.hudCurrentVelocity.position.set(
      Math.cos( velocityAngle ) * 200,
      Math.sin( velocityAngle ) * 200,
      0
    );

    this.hudCurrentVelocity.material.rotation = velocityAngle;
}
    // TODO: Aquí realizamos la proyección de los asteroides
    // sobre el radar. Sería MUY interesante realizar la
    // proyección de todos los objetos que vayan a aparecer
    // en escena.
    const width = this.canvas.width * 0.5;
    const height = this.canvas.height * 0.5;

    const frontRadarX = width - HUD_RADAR_POSITION;
    const backRadarX = -width + HUD_RADAR_POSITION;
    const radarY = -height + HUD_RADAR_POSITION;

    this.hudFront.position.set(frontRadarX, radarY, 0);
    this.hudBack.position.set(backRadarX, radarY, 0);
    this.hudStatus.position.set(frontRadarX, height - (HUD_RADAR_HALFSIZE + 32), 0);

    /**
     * Actualizamos el debris.
     */
    this.debris.update( this.ship.target );

    /**
     * Actualizamos los proyectiles.
     */
    this.projectiles.update();

    /**
     * Actualizamos los asteroides.
     */
    for (let index = 0; index < this.asteroids.length; index++) {
      const asteroid = this.asteroids[index];
      asteroid.rotation.x += asteroid.userData.rotX;
      asteroid.rotation.y += asteroid.userData.rotY;
      asteroid.update(this.camera);
      const hudAsteroid = this.hudAsteroids[index];

      // COMPROBAMOS SI EL JUGADOR COLISIONA
      // CONTRA UN ASTEROIDE.
      if (this.ship.target.position.distanceTo(asteroid.position) < asteroid.scale.x) {
        const mat4 = new THREE.Matrix4();
        const dir = new THREE.Vector3(0,0,1);
        const up = new THREE.Vector3(0,1,0);
        mat4.identity();
        mat4.lookAt(asteroid.position, this.ship.target.position, up);
        dir.applyMatrix4(mat4);
        this.ship.target.translateOnAxis(dir, -10);
        this.ship.collision(dir);
        this.addAsteroidHitSound();
        //console.log("COLLIDED!");
      }

      const position = asteroid.position.clone();
      position.project(this.ship.target);

      // WTF! No entiendo por qué debo invertir los valores
      // de las coordenadas para ambas coordenadas.
      const px = Math.min(1.0, Math.max(-1.0,-position.x * 0.5));
      const py = Math.min(1.0, Math.max(-1.0,-position.y * 0.5));

      // convertimos las coordenadas de la pantalla en coordenadas polares.
      const d = Math.sqrt(px * px + py * py);
      const a = Math.atan2(py,px);
      const lx = Math.cos(a) * Math.min(HUD_RADAR_LIMIT, d);
      const ly = Math.sin(a) * Math.min(HUD_RADAR_LIMIT, d);

      const x = lx * HUD_RADAR_HALFSIZE;
      const y = ly * HUD_RADAR_HALFSIZE;
      if (position.z > 1.0) {
        hudAsteroid.position.set( backRadarX + x, radarY + y, 0);
      } else {
        hudAsteroid.position.set( frontRadarX + x, radarY + y, 0);
      }
    }

    /**
     * Actualizamos las inteligencias artificiales
     * que a su vez actualizan las naves.
     */
    for (let index = this.ships.length - 1; index >= 0; index--) {
      const ship = this.ships[index];
      //const oldId = this.getTileId(ship.target.target);
      const isDestroyed = ship.update(this.camera);
      //const newId = this.getTileId(ship.target.target);

      /*if (oldId !== newId) {
        this.removeFromTile(ship.target.target, oldId);
      }
      this.addToTile(ship.target.target, newId);*/

      const hudShip = this.hudShips[index];

      const position = ship.target.target.position.clone();
      position.project(this.ship.target);

      // WTF! No entiendo por qué debo invertir los valores
      // de las coordenadas para ambas coordenadas.
      // WTF! No entiendo por qué debo invertir los valores
      // de las coordenadas para ambas coordenadas.
      const px = Math.min(1.0, Math.max(-1.0,-position.x * 0.5));
      const py = Math.min(1.0, Math.max(-1.0,-position.y * 0.5));

      // convertimos las coordenadas de la pantalla en coordenadas polares.
      const d = Math.sqrt(px * px + py * py);
      const a = Math.atan2(py,px);
      const lx = Math.cos(a) * Math.min(HUD_RADAR_LIMIT, d);
      const ly = Math.sin(a) * Math.min(HUD_RADAR_LIMIT, d);

      const x = lx * HUD_RADAR_HALFSIZE;
      const y = ly * HUD_RADAR_HALFSIZE;
      if (position.z > 1.0) {
        hudShip.position.set( backRadarX + x, radarY + y, 0);
      } else {
        hudShip.position.set( frontRadarX + x, radarY + y, 0);
      }

      const hudMarker = this.hudMarkers[index];
      const hx = (position.z > 1.0 ? this.hudCrossHair.position.x : -position.x * width);
      const hy = (position.z > 1.0 ? this.hudCrossHair.position.y : -position.y * height);
      hudMarker.position.set(
        hudMarker.position.x + ((hx - hudMarker.position.x) * 0.35),
        hudMarker.position.y + ((hy - hudMarker.position.y) * 0.35),
        0
      );

      if (!isDestroyed) {
        const [removed] = this.ships.splice( index, 1 );
        //this.removeFromTile( ship.target.target, newId );
        this.scene.remove( ship.target.target );
        this.hud.remove( hudShip );
      }
    }
  }

  render() {
    this.renderer.render( this.scene, this.camera );

    // esto renderiza el hud (aunque no he conseguido que funcione).
	  this.renderer.autoClear = false;
	  this.renderer.clear(false, true, true);
	  this.renderer.render( this.hud, this.hudCamera, undefined, false);
	  this.renderer.autoClear = true;
  }

  resize() {
    this.renderer.setSize( this.canvas.clientWidth, this.canvas.clientHeight, false );

    // necesitamos actualizar todas las cámaras.
    // si el objeto cámara poseyera un setter, se podría
    // controlar si la cámara necesita un refresco del aspect
    // ratio.
    this.camera.aspect = this.canvas.width / this.canvas.height;
    this.camera.updateProjectionMatrix();

    const adjustWidth = true;
    // renderizamos el HUD.
    const aspect = adjustWidth ? this.canvas.width / this.canvas.height : this.canvas.height / this.canvas.width;
    if (adjustWidth) {
      this.hudCamera.left = this.hudCamera.bottom * aspect;
      this.hudCamera.right = this.hudCamera.top * aspect;
    } else {
      this.hudCamera.bottom = this.hudCamera.left * aspect;
      this.hudCamera.top = this.hudCamera.right * aspect;
    }
    this.hudCamera.width = this.hudCamera.right - this.hudCamera.left;
    this.hudCamera.height = this.hudCamera.bottom - this.hudCamera.top;
    this.hudCamera.updateProjectionMatrix();
  }

  beforeUnload(e) {
    e.returnValue = false;
  }
}

window.THREE = THREE;
const game = window.game = new Game();
