export const Key = (function() {

  const KEYS = 256;
  const keys = new Array(KEYS);
  for (let index = 0; index < KEYS; index++) {
    keys[index] = false;
  }

  function handler(e) {
    keys[e.keyCode] = (e.type === "keydown");
  }

  function isPressed(key) {
    return keys[key];
  }

  function isReleased(key) {
    return !keys[key];
  }

  window.addEventListener("keyup", handler, false);
  window.addEventListener("keydown", handler, false);

  return {
    BACKSPACE: 8,
    ENTER: 13,
    ESCAPE: 27,
    SPACE: 32,

    LEFT: 37,
    UP: 38,
    RIGHT: 39,
    DOWN: 40,

    NUM_0: 48,
    NUM_1: 49,
    NUM_2: 50,
    NUM_3: 51,
    NUM_4: 52,
    NUM_5: 53,
    NUM_6: 54,
    NUM_7: 55,
    NUM_8: 56,
    NUM_9: 57,

    A: 65,
    B: 66,
    C: 67,
    D: 68,
    E: 69,
    F: 70,
    G: 71,
    H: 72,
    I: 73,
    J: 74,
    K: 75,
    L: 76,
    M: 77,
    N: 78,
    O: 79,
    P: 80,
    Q: 81,
    R: 82,
    S: 83,
    T: 84,
    U: 85,
    V: 86,
    W: 87,
    X: 88,
    Y: 89,
    Z: 90,

    isPressed,
    isReleased
  };

})();

export default Key;
