import {PROJECTILE_VELOCITY, PROJECTILE_DURATION} from "./constants";

export class Projectile {
  constructor(target) {
    this.target = target;
    this.startTime = Date.now();
    this.currentTime = Date.now();
  }

  update(game) {
    this.target.translateZ( -PROJECTILE_VELOCITY );

    for (let asteroid of game.asteroids) {
      if (this.target.position.distanceTo(asteroid.position) <= asteroid.scale.x) {
        return 0;
      }
    }

    this.currentTime = Date.now();
    if (this.currentTime - this.startTime > PROJECTILE_DURATION) {
      return false;
    }
    return true;
  }
}

export default Projectile;
