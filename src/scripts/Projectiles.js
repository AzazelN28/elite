import THREE from "three";
import {PATH} from "./constants";
import Projectile from "./Projectile";

export class Projectiles {
  constructor(scene, game) {
    this.scene = scene;
    this.game = game;
    this.projectiles = [];

    this.material = new THREE.SpriteMaterial({
      map: (new THREE.TextureLoader().setPath(PATH).load("fire.png")),
      color: 0xffffee,
      blending: THREE.AdditiveBlending
    });
  }

  create(ship) {
    {
      const projectileSprite = new THREE.Sprite(this.material);
      projectileSprite.material.rotation = Math.random() * Math.PI * 2;
      projectileSprite.position.copy(ship.target.position);
      projectileSprite.quaternion.copy(ship.target.quaternion);

      projectileSprite.translateY( -1 );
      projectileSprite.translateX( -1 );

      const projectile = new Projectile( projectileSprite );

      this.projectiles.push( projectile );
      this.scene.add( projectileSprite );
    }

    {
      const projectileSprite = new THREE.Sprite(this.material);
      projectileSprite.material.rotation = Math.random() * Math.PI * 2;
      projectileSprite.position.copy(ship.target.position);
      projectileSprite.quaternion.copy(ship.target.quaternion);

      projectileSprite.translateY( -1 );
      projectileSprite.translateX( 1 );

      const projectile = new Projectile( projectileSprite );

      this.projectiles.push( projectile );
      this.scene.add( projectileSprite );
    }
  }

  update() {
    // Actualiza todos los proyectiles que haya en la escena. Se podrían crear
    // grupos para controlar misiles, etc.
    for (let index = this.projectiles.length - 1; index >= 0; index--) {
      const projectile = this.projectiles[index];
      //const oldId = this.getTileId(projectile.target);
      const continues = projectile.update(this.game);
      //const newId = this.getTileId(projectile.target);

      /*if (oldId !== newId) {
        this.removeFromTile(projectile.target, oldId);
      }
      this.addToTile(projectile.target, newId);*/

      if (continues !== true) {
        const [removed] = this.projectiles.splice( index, 1 );
        //this.removeFromTile( projectile.target, newId );
        this.scene.remove( projectile.target );

        if (continues === 0) {
          //console.log("EXPLOSION!");
          // TODO: Esto añade un sprite a la escena
          // pero este sprite todavía no tiene nada
          // debería ser una explosion.
          /*const sprite = new THREE.Sprite(
            new THREE.SpriteMaterial({
              color: 0xff0000
            })
          );
          sprite.position.set(
            projectile.target.position.x,
            projectile.target.position.y,
            projectile.target.position.z
          );
          this.scene.add(sprite);*/
        }
      }
    }
  }

}

export default Projectiles;
