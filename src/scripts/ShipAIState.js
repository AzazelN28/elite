
export const ShipAIState = {
  DEFAULT: "default",
  WAITING: "waiting",
  TARGETING: "targeting"
};

export default ShipAIState;
