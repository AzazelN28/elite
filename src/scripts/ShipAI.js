import THREE from "three";
import {MIN_FIRE_DISTANCE, NEGATIVE} from "./constants";
import ShipAIState from "./ShipAIState";


function dot(a,b) {
  return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
}

function almostEquals(a,b,epsilon = 0.01) {
  const matching = dot(a,b);
  return (Math.abs(matching - 1.0) < epsilon);
}

function angleBetween(a,b) {
  return Math.acos(dot(a,b));
}

function isInRange(a,b,angle) {
  const ang = Math.acos(dot(a,b));
  return (ang >= -angle && ang <= angle);
}

export class ShipAI {
  constructor(target, game) {
    this.target = target;
    this.state = ShipAIState.DEFAULT;
    this.player = game.ship.target;
    this.projectiles = game.projectiles;
    this.game = game;

    // TODO: Esto es una AUTÉNTICA MIERDA, estoy seguro de que investigando
    // un poco más sobre el algebra de grassman y sobre Quaterniones aprenderé
    // cómo resolver este problema mucho más fácilmente.
    this.playerMatrix = new THREE.Matrix4();
    this.playerQuaternion = new THREE.Quaternion();
    this.playerVectorUp = new THREE.Vector3(0,1,0);
  }

  update(camera) {
    const vecUp = new THREE.Vector3(0,1,0);

    // TODO: Aquí deberíamos computar cuáles son las acciones
    // con un árbol de decisiones o una máquina de estados.
    switch(this.state) {
      case ShipAIState.DEFAULT:
        if (this.target.isNotFullThrottled) {
          this.target.move(NEGATIVE);
        }

        this.playerMatrix.identity();
        this.playerMatrix.lookAt(this.target.target.position, this.player.position, this.playerVectorUp);
        this.playerQuaternion.setFromRotationMatrix(this.playerMatrix);

        if (isInRange(this.playerQuaternion, this.target.target.quaternion, Math.PI * 0.125) && this.target.fire() && this.target.target.position.distanceTo(this.player.position) < MIN_FIRE_DISTANCE) {
          this.game.addProjectiles(this.target, Math.min(1.0, Math.max(0.1, 1.0 - (this.target.target.position.distanceTo(this.player.position) * 0.001))));
          //console.log("FIRE!");
        }

        break;
    }

    this.target.update();

    // CHAPUZACA
    this.target.target.quaternion.slerp(this.playerQuaternion, 0.005);
    this.target.target.update(camera);
    return true;

  }
}

export default ShipAI;
