import THREE from "three";
import ShipModel from "./ShipModel";
import {AXIS_X,AXIS_Y,AXIS_Z} from "./constants";

export class Ship {
  constructor(target) {
    this.target = target;

    this.yawQuaternion = new THREE.Quaternion();
    this.rollQuaternion = new THREE.Quaternion();
    this.pitchQuaternion = new THREE.Quaternion();

    this.angularVelocity = new THREE.Vector3();
    this.currentVelocity = new THREE.Vector3();
    this.targetVelocity = new THREE.Vector3();

    this.fireStartTime = Date.now();
    this.fireCurrentTime = Date.now();

    this.boostStartTime = Date.now();
    this.boostCurrentTime = Date.now();

    // indica la cantidad de carga que lleva
    // la nave en la bodega.
    this.ship = new ShipModel();
    this.collisionStartTime = Date.now();
    this.collisionCurrentTime = Date.now();
    this.collided = false;
    //this.collider = new THREE.Sphere(this.target.position, 5);
  }

  get relativeTargetVelocity() {
    return this.targetVelocity.z / this.ship.maxVelocity;
  }

  get relativeCurrentVelocity() {
    return this.currentVelocity.z / this.ship.maxVelocity;
  }

  get isStopped() {
    return Math.abs(this.targetVelocity.z) < 0.1 && Math.abs(this.currentVelocity.z) < 0.1;
  }

  get isStopping() {
    return Math.abs(this.targetVelocity.z) < 0.1 && Math.abs(this.currentVelocity.z) >= 0.1;
  }

  get isNotFullThrottled() {
    return this.targetVelocity.z < this.ship.maxVelocity;
  }

  get isFullThrottled() {
    return this.targetVelocity.z === this.ship.maxVelocity;
  }

  fire() {
    this.fireCurrentTime = Date.now();
    if (this.fireCurrentTime - this.fireStartTime > 100) {
      this.fireCurrentTime = this.fireStartTime = Date.now();
      return true;
    }
    return false;
  }

  yaw(m) {
    this.angularVelocity.x += this.ship.angularAcceleration.x * m;
  }

  pitch(m) {
    this.angularVelocity.y += this.ship.angularAcceleration.y * m;
  }

  roll(m) {
    this.angularVelocity.z += this.ship.angularAcceleration.z * m;
  }

  strafeX(m) {
    const newValue = this.targetVelocity.x + this.ship.targetAcceleration.x * m;
    if (Math.abs(newValue) <= this.ship.maxVelocity) {
      this.targetVelocity.x = newValue;
    }
  }

  strafeY(m) {
    const newValue = this.targetVelocity.y + this.ship.targetAcceleration.y * m;
    if (Math.abs(newValue) <= this.ship.maxVelocity) {
      this.targetVelocity.y = newValue;
    }
  }

  move(m) {
    const newValue = this.targetVelocity.z + this.ship.targetAcceleration.z * m;
    if (Math.abs(newValue) <= this.ship.maxVelocity) {
      this.targetVelocity.z = newValue;
    }
  }

  boost() {
    this.boostCurrentTime = Date.now();
    if (this.boostCurrentTime - this.boostStartTime > 1000) {
      this.currentVelocity.z = this.ship.boost;
    }
  }

  accelerate(currentVelocity, targetVelocity, acceleration) {

    if (currentVelocity < targetVelocity) {
      if (currentVelocity + acceleration < targetVelocity) {
        return currentVelocity + acceleration;
      } else {
        return targetVelocity;
      }
    } else if (currentVelocity > targetVelocity) {
      if (currentVelocity - acceleration > targetVelocity) {
        return currentVelocity - acceleration;
      } else {
        return targetVelocity;
      }
    }

    return currentVelocity;

  }

  collision(dir) {

    this.collided = true;
    this.collisionCurrentTime = this.collisionStartTime = Date.now();

    this.angularVelocity.x = (Math.random() - 0.5) * 2;
    this.angularVelocity.y = (Math.random() - 0.5) * 2;
    this.angularVelocity.z = (Math.random() - 0.5) * 5;

    this.currentVelocity.x = dir.x * 5;
    this.currentVelocity.y = dir.y * 5;
    this.currentVelocity.z = dir.z * 5;

  }

  update() {

    // TODO: Aquí se podría poner un booleano
    // que indique si está el "Flight assist"
    // activado (que básicamente lo que hará
    // es utilizar OTRO vector para indicar
    // e

    // velocidad actual.
    this.currentVelocity.x = this.accelerate(this.currentVelocity.x, this.targetVelocity.x, this.ship.linearAcceleration.x);
    this.currentVelocity.y = this.accelerate(this.currentVelocity.y, this.targetVelocity.y, this.ship.linearAcceleration.y);
    this.currentVelocity.z = this.accelerate(this.currentVelocity.z, this.targetVelocity.z, this.ship.linearAcceleration.z);

    // Aquí aplicamos fricción al movimiento lateral y vertical (aunque en realidad
    // se podría aplicar a todas)
    this.targetVelocity.x *= this.ship.linearFriction.x;
    this.targetVelocity.y *= this.ship.linearFriction.y;
    //this.targetVelocity.z *= this.ship.linearFriction.z;

    // TODO: Todos estos valores son parametrizables.
    if (this.collided) {
      this.collisionCurrentTime = Date.now();
      if (this.collisionCurrentTime - this.collisionStartTime > 5000) {
        this.collided = false;
      }
      this.angularVelocity.x *= 0.99;
      this.angularVelocity.y *= 0.99;
      this.angularVelocity.z *= 0.99;
    } else {
      this.angularVelocity.x *= this.ship.angularFriction.x;
      this.angularVelocity.y *= this.ship.angularFriction.y;
      this.angularVelocity.z *= this.ship.angularFriction.z;
    }

    // Esta parte controla el movimiento al estilo de un avión.
    this.yawQuaternion.setFromAxisAngle( AXIS_X, this.angularVelocity.x );
    this.pitchQuaternion.setFromAxisAngle( AXIS_Y, this.angularVelocity.y );
    this.rollQuaternion.setFromAxisAngle( AXIS_Z, this.angularVelocity.z );

    this.target.quaternion.multiply( this.yawQuaternion );
    this.target.quaternion.multiply( this.pitchQuaternion );
    this.target.quaternion.multiply( this.rollQuaternion );

    // Esta parte controla el movimiento de la nave, tanto hacia
    // adelante como el movimiento lateral y vertical.
    this.target.translateX(this.currentVelocity.x);
    this.target.translateY(this.currentVelocity.y);
    this.target.translateZ(this.currentVelocity.z);

    //this.collider.center.set(this.target.position);

  }

}

export default Ship;
