import THREE from "three";
import {DEBRIS, DEBRIS_DISTANCE, MAIN_LIGHT_COLOR, PATH, AXIS_X, AXIS_Y, AXIS_Z} from "./constants";

export class Debris {
  constructor(scene, count = DEBRIS, distance = DEBRIS_DISTANCE) {
    this.scene = scene;
    this.debris = [];
    this.distance = distance;
    this.material = new THREE.SpriteMaterial({
      color: MAIN_LIGHT_COLOR,
      map: (new THREE.TextureLoader().setPath(PATH).load("dust.png"))
    });

    // Esta parte se encarga de actualizar el debris.
    this.qx = new THREE.Quaternion();
    this.qy = new THREE.Quaternion();
    this.qz = new THREE.Quaternion();

    // Creamos el debris, esto sería muy molón si pudiera ser o bien líneas
    // que se van alargando según aumenta la velocidad o bien, mierda dando
    // vueltas como ocurría en el X-Wing
    for (let index = 0; index < count; index++) {
      const debrisMaterial = this.material.clone();
      debrisMaterial.rotation = Math.random() * 2 * Math.PI;
      debrisMaterial.transparent = true;
      debrisMaterial.opacity = 0;
      const debris = new THREE.Sprite( debrisMaterial );
      const x = ((Math.random() - 0.5) * 2.0) * this.distance;
      const y = ((Math.random() - 0.5) * 2.0) * this.distance;
      const z = ((Math.random() - 0.5) * 2.0) * this.distance;
      debris.position.set( x, y, z );
      const s = (Math.random() + 1) * 0.5;
      debris.scale.set(s,s,s);
      this.scene.add( debris );
      this.debris.push( debris );
    }
  }

  update(target) {
    for (let debris of this.debris) {
      if (debris.position.distanceTo( target.position ) > this.distance) {
        debris.position.copy( target.position );
        this.qx.setFromAxisAngle( AXIS_X, Math.random() * 2 * Math.PI );
        this.qy.setFromAxisAngle( AXIS_Y, Math.random() * 2 * Math.PI );
        this.qz.setFromAxisAngle( AXIS_Z, Math.random() * 2 * Math.PI );
        debris.quaternion.multiply(this.qx);
        debris.quaternion.multiply(this.qy);
        debris.quaternion.multiply(this.qz);
        debris.translateZ( -this.distance );
      }
      const d = 1.0 - (debris.position.distanceTo(target.position) / this.distance);
      debris.material.opacity = Math.max( 0.0, Math.min( 1.0, d ) );
    }
  }

  dispose() {
    for (let debris of this.debris) {
      this.scene.remove( debris );
    }
  }
}

export default Debris;
