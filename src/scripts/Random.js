const INCREMENT = 12345;
const MULTIPLIER = 1103515245;
const MODULUS = 2147483648;

let seed = 0;

function lcg(value, increment, multiplier, modulus) {
  return (((value + increment) * multiplier) % modulus);
}

export function value() {
  seed = lcg(seed, INCREMENT, MULTIPLIER, MODULUS);
  return seed / MODULUS;
}

export function unit() {
  return (value() - 0.5) * 2.0;
}

export function angle() {
  return unit() * Math.PI;
}

export function reset(newSeed) {
  seed = newSeed;
}

export function between(min, max) {
  return (value() * (max - min)) + min;
}

export function intBetween(min, max) {
  return Math.round(between(min, max));
}

