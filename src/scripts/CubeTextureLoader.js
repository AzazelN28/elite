import THREE from "three";

export class CubeTextureLoader {

  constructor(manager = THREE.DefaultLoadingManager) {
    this.manager = manager;
    this.texture = new THREE.CubeTexture();
  }

  load(url, onLoad, onProgress, onError) {

    const texture = this.texture;
    const loader = new THREE.ImageLoader( this.manager );
    loader.setCrossOrigin(this.crossOrigin);
    loader.setPath(this.path);
    loader.load( url, (image) => {

      const size = image.naturalHeight;

      const posx = document.createElement("canvas");
      posx.width = posx.height = size;
      const posxc = posx.getContext("2d");

      const negx = document.createElement("canvas");
      negx.width = negx.height = size;
      const negxc = negx.getContext("2d");

      const posy = document.createElement("canvas");
      posy.width = posy.height = size;
      const posyc = posy.getContext("2d");

      const negy = document.createElement("canvas");
      negy.width = negy.height = size;
      const negyc = negy.getContext("2d");

      const posz = document.createElement("canvas");
      posz.width = posz.height = size;
      const poszc = posz.getContext("2d");

      const negz = document.createElement("canvas");
      negz.width = negz.height = size;
      const negzc = negz.getContext("2d");

      const W0 = size * 0;
      const W1 = size * 1;
      const W2 = size * 2;
      const W3 = size * 3;
      const W4 = size * 4;
      const W5 = size * 5;

      posxc.drawImage(image, W0, 0, size, size, 0, 0, size, size);
      negxc.drawImage(image, W1, 0, size, size, 0, 0, size, size);
      posyc.drawImage(image, W2, 0, size, size, 0, 0, size, size);
      negyc.drawImage(image, W3, 0, size, size, 0, 0, size, size);
      poszc.drawImage(image, W4, 0, size, size, 0, 0, size, size);
      negzc.drawImage(image, W5, 0, size, size, 0, 0, size, size);

      texture.images[0] = posx;
      texture.images[1] = negx;
      texture.images[2] = posy;
      texture.images[3] = negy;
      texture.images[4] = posz;
      texture.images[5] = negz;
      texture.needsUpdate = true;

      //console.log("loaded background");
      if (onLoad) {
        onLoad(texture);
      }

    }, onProgress, onError );

  }

  setCrossOrigin(value) {
    this.crossOrigin = value;
    return this;
  }

  setPath(value) {
    this.path = value;
    return this;
  }

}

export default CubeTextureLoader;
