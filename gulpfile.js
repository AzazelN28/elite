const gulp = require("gulp"),
      plugins = require("gulp-load-plugins")(),
      source = require("vinyl-source-stream"),
      buffer = require("vinyl-buffer"),
      browserify = require("browserify"),
      bs = require("browser-sync");

gulp.task("templates", () => {

  return gulp.src("src/templates/index.pug")
    .pipe(plugins.pug({
      locals: {
        title: "Code Red 2",
        type: "website",
        description: "WebGL Space Combat Simulator",
        url: "http://rojo2.com/labs/webgl/elite/",
        image: "http://rojo2.com/labs/webgl/elite/cover.png",
        twitter: "@rojo_2"
      }
    }))
    .pipe(gulp.dest("dist"));

});

gulp.task("styles", () => {

  return gulp.src("src/styles/index.styl")
    .pipe(plugins.stylus({
      compress: false
    }))
    .pipe(gulp.dest("dist"));

});

gulp.task("scripts", () => {

  return browserify()
    .add("src/scripts/index.js")
    .transform("babelify", { presets: ["es2015", "react"] })
    .transform("uglifyify", { global: true })
    .bundle()
    .pipe(source("index.js"))
    .pipe(gulp.dest("dist"));

});

gulp.task("build", ["scripts", "styles", "templates"]);

gulp.task("watch", ["build"], () => {

  gulp.watch("src/scripts/**/*.js", ["scripts"]);
  gulp.watch("src/styles/**/*.js", ["styles"]);
  gulp.watch("src/templates/**/*.js", ["templates"]);

});

gulp.task("bs", ["watch"], () => {

  bs.init({
    server: {
      baseDir: "dist"
    }
  });

});

gulp.task("default", ["bs"]);

