# TO DO

- [ ] Show a preloader to load assets
- [ ] Add collision ship / ship
- [ ] Add collision ship / projectile
- [ ] Add collision ship (enemies) / asteroid
- [ ] Add explosions on shot hits enemy
- [ ] Add sand burst on shot hits asteroid
- [ ] Add gamepad layout setup
- [ ] Add keyboard layout setup

# Optimizations

- [ ] Replace Math.sqrt whenever it's possible
- [ ] Replace Math.cos and Math.sin whenever it's possible

## Things to research

- http://www.cannonjs.org/
- https://dev.twitter.com/cards/overview
- https://es.wikipedia.org/wiki/RDFa
- https://developers.google.com/+/web/+1button/#plus-snippet

