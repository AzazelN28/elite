# Game Design Document

## Ambientación

El juego estará ambientado en una representación realista del sistema solar, con sus asteroides
troyanos, su cinturón de asteroides, sus planetas, lunas, etc. donde corporaciones enormes luchan
por el control y la gestión de los recursos de este sistema.

Los jugadores tomarán el papel de un piloto de nave recién examinado, como quién se saca el carné
de conducir.


