# Elite

Juego de combate espacial entre dos naves.

## Disposición del teclado

| Teclas                            | Acción
| :---:                             | :---
| <kbd>W</kbd>, <kbd>S</kbd>        | Aumentar y disminuir velocidad
| <kbd>A</kbd>, <kbd>D</kbd>        | Deslizarse hacia la izquierda y hacia la derecha
| <kbd>Q</kbd>, <kbd>E</kbd>        | Rotar en el eje Y (pitch)
| <kbd>R</kbd>, <kbd>F</kbd>        | Deslizarse hacia arriba y hacia abajo
| <kbd>UP</kbd>, <kbd>DOWN</kbd>    | Rotar en el eje X (yaw)
| <kbd>LEFT</kbd>, <kbd>RIGHT</kbd> | Rotar en el eje Z (roll)
| <kbd>SPACE</kbd>                  | Disparar

## Disposición del gamepad

_TODO_
